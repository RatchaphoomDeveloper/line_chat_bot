var express = require("express");
var router = express.Router();
const lineconn = require("../configs/conf");
const axios = require("axios");
const { CHANEL_ID, LINE_AUTHOR } = process.env;
/* GET home page. */
router.post("/sendlines", async function (req, res, next) {
  const { messages } = req.body;

  await axios
    .post(
      "https://api.line.me/v2/bot/message/broadcast",
      {
        to: CHANEL_ID,
        messages: messages
      },
      {
        headers: {
          Authorization: `Bearer ${LINE_AUTHOR}`
        },
      }
    )
    .then((ress) => {
      console.log(res.data);
      return res.status(200).json({ message: "success" });
    })
    .catch((error) => {
      console.error(error);
      return res.status(400).json({ message: "fail" });
    });

});

module.exports = router;
